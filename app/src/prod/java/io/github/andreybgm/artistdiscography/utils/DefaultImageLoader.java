package io.github.andreybgm.artistdiscography.utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import io.github.andreybgm.artistdiscography.data.Album;

public class DefaultImageLoader implements ImageLoader {
    @Override
    public void loadAlbumCover(Context context, Album album, ImageView imageView) {
        String imageUrl = album.getImageUrl();

        if (imageUrl == null || imageUrl.isEmpty()) {
            imageView.setImageResource(android.R.color.transparent);
            return;
        }

        Glide.with(context)
                .load(imageUrl)
                .centerCrop()
                .into(imageView);
    }
}
