package io.github.andreybgm.artistdiscography;

import android.support.annotation.NonNull;

import io.github.andreybgm.artistdiscography.utils.DefaultImageLoader;
import io.github.andreybgm.artistdiscography.utils.ImageLoader;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

import static okhttp3.logging.HttpLoggingInterceptor.Level;

public class Injection implements InjectionContract {

    private static final Injection injection = new Injection();

    private final OkHttpClient okHttpClient;
    private final DefaultImageLoader imageLoader;

    @NonNull
    public static Injection provideInjection() {
        return injection;
    }

    private Injection() {
        this.okHttpClient = createOkHttpClient();
        this.imageLoader = new DefaultImageLoader();
    }

    @Override
    @NonNull
    public OkHttpClient provideOkHttpClient() {
        return okHttpClient;
    }

    @NonNull
    @Override
    public ImageLoader provideImageLoader() {
        return imageLoader;
    }

    @NonNull
    private static OkHttpClient createOkHttpClient() {
        Level logLevel = BuildConfig.DEBUG ? Level.BASIC : Level.NONE;
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor().setLevel(logLevel);

        return new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build();
    }
}
