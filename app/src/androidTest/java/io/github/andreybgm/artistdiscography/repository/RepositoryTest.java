package io.github.andreybgm.artistdiscography.repository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import io.github.andreybgm.artistdiscography.data.Album;
import io.github.andreybgm.artistdiscography.repository.api.MockingInterceptor;

import static org.assertj.core.api.Assertions.assertThat;

public class RepositoryTest {

    private static final String ARTIST = "In Flames";

    private Album album1;
    private Album album2;
    private Repository repository;

    @Before
    public void setUp() throws Exception {
        album1 = new Album("Come Clarity",
                "http://is1.mzstatic.com/image/thumb/Music62/926/source/100x100bb.jpg");
        album2 = new Album("Reroute to Remain",
                "http://is5.mzstatic.com/image/thumb/Music22/a359ef4d/source/100x100bb.jpg");
        repository = new Repository();
    }

    @After
    public void tearDown() throws Exception {
        MockingInterceptor.setShouldReturnError(false);
    }

    @Test
    public void findAlbums() throws Exception {
        int expectedAlbumCount = 2;

        repository.findAlbumsByArtist(ARTIST)
                .test()
                .await()
                .assertNoErrors()
                .assertComplete()
                .assertValue(albums -> {
                    assertThat(albums)
                            .hasSize(expectedAlbumCount)
                            .containsExactly(album1, album2);

                    return true;
                });
    }

    @Test
    public void findAlbumsWhenNetworkError() throws Exception {
        MockingInterceptor.setShouldReturnError(true);

        repository.findAlbumsByArtist(ARTIST)
                .test()
                .await()
                .assertError(t -> true);
    }
}