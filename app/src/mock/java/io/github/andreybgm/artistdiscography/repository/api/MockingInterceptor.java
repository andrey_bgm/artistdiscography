package io.github.andreybgm.artistdiscography.repository.api;

import android.content.res.AssetManager;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Pattern;

import io.github.andreybgm.artistdiscography.DiscographyApplication;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;

public class MockingInterceptor implements Interceptor {

    private static final String LOG_TAG = MockingInterceptor.class.getSimpleName();
    private static final MediaType TYPE_TEXT_JAVASCRIPT = MediaType.parse("text/javascript");
    private static final String RESPONSE_FOLDER = "api/response/";

    private static volatile boolean shouldReturnError;

    private static final Pattern SEARCH_ALBUM_PATTERN = Pattern.compile(".*/search\\?.*term=(.+)");

    private final PathHandler<String> pathHandler;

    public static Interceptor newInstance() {
        return new MockingInterceptor();
    }

    private MockingInterceptor() {
        pathHandler = PathHandler.Builder.<String>create()
                .patternHandler(SEARCH_ALBUM_PATTERN, PathHandler.Values.<String>create()
                        .value(RESPONSE_FOLDER + "inflames.json", "In%20Flames")
                )
                .build();
    }

    public static void setShouldReturnError(boolean shouldReturnError) {
        MockingInterceptor.shouldReturnError = shouldReturnError;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        String url = request.url().toString();

        Log.d(LOG_TAG, String.format("Got the request %s", url));

        if (shouldReturnError) {
            return createErrorResponse(request, 500, "The synthetic network error");
        }

        String assetFileName = pathHandler.findValue(url);

        if (assetFileName != null) {
            return createResponseFromAsset(request, assetFileName);
        }

        throw new IllegalArgumentException(String.format("Cannot mock the request %s", url));
    }

    private Response createResponseFromAsset(Request request, String fileName) {
        AssetManager assets = DiscographyApplication.getContext().getAssets();

        try (InputStream input = assets.open(fileName)) {
            return createSuccessResponse(request, input);
        } catch (IOException e) {
            return createErrorResponse(request, 500, e.getMessage());
        }
    }

    private Response createSuccessResponse(Request request, InputStream input) throws IOException {
        Buffer buffer = new Buffer().readFrom(input);

        return new Response.Builder()
                .request(request)
                .protocol(Protocol.HTTP_1_1)
                .code(200)
                .message("OK")
                .body(ResponseBody.create(TYPE_TEXT_JAVASCRIPT, buffer.size(), buffer))
                .build();
    }

    private Response createErrorResponse(Request request, int code, String message) {
        return new Response.Builder()
                .request(request)
                .protocol(Protocol.HTTP_1_1)
                .code(code)
                .message(message)
                .body(ResponseBody.create(TYPE_TEXT_JAVASCRIPT, ""))
                .build();
    }
}
