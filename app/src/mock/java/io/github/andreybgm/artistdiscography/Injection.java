package io.github.andreybgm.artistdiscography;

import android.support.annotation.NonNull;

import io.github.andreybgm.artistdiscography.repository.api.MockingInterceptor;
import io.github.andreybgm.artistdiscography.utils.FakeImageLoader;
import io.github.andreybgm.artistdiscography.utils.ImageLoader;
import okhttp3.OkHttpClient;

public class Injection implements InjectionContract {

    private static final Injection injection = new Injection();
    private final ImageLoader imageLoader;
    private final OkHttpClient okHttpClient;

    @NonNull
    public static Injection provideInjection() {
        return injection;
    }

    private Injection() {
        this.imageLoader = new FakeImageLoader();
        this.okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(MockingInterceptor.newInstance())
                .build();
    }

    @Override
    @NonNull
    public OkHttpClient provideOkHttpClient() {
        return okHttpClient;
    }

    @NonNull
    @Override
    public ImageLoader provideImageLoader() {
        return imageLoader;
    }
}
