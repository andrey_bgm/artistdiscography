package io.github.andreybgm.artistdiscography.utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import io.github.andreybgm.artistdiscography.R;
import io.github.andreybgm.artistdiscography.data.Album;

public class FakeImageLoader implements ImageLoader {
    @Override
    public void loadAlbumCover(Context context, Album album, ImageView imageView) {
        Glide.with(context)
                .load(R.drawable.fake_album_cover)
                .centerCrop()
                .into(imageView);
    }
}
