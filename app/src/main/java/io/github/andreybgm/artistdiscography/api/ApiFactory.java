package io.github.andreybgm.artistdiscography.api;

import io.github.andreybgm.artistdiscography.BuildConfig;
import io.github.andreybgm.artistdiscography.Injection;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiFactory {

    private static ITunesService service;

    private ApiFactory() {
    }

    public static synchronized ITunesService getService() {
        if (service == null) {
            service = createRetrofit().create(ITunesService.class);
        }

        return service;
    }

    private static Retrofit createRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.API_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(Injection.provideInjection().provideOkHttpClient())
                .build();
    }
}
