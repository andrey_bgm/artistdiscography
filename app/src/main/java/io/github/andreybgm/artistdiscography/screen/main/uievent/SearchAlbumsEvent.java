package io.github.andreybgm.artistdiscography.screen.main.uievent;

import io.github.andreybgm.artistdiscography.screen.base.UiEvent;

public class SearchAlbumsEvent implements UiEvent {
    private final String artist;

    public static SearchAlbumsEvent create(String artist) {
        return new SearchAlbumsEvent(artist);
    }

    private SearchAlbumsEvent(String artist) {
        this.artist = artist;
    }

    public String getArtist() {
        return artist;
    }
}
