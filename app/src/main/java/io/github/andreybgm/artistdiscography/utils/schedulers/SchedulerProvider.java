package io.github.andreybgm.artistdiscography.utils.schedulers;

import io.reactivex.Scheduler;

public interface SchedulerProvider {
    Scheduler ui();

    Scheduler io();

    Scheduler computation();
}
