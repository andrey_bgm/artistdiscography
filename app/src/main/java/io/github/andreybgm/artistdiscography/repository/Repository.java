package io.github.andreybgm.artistdiscography.repository;

import java.util.List;

import io.github.andreybgm.artistdiscography.api.ApiFactory;
import io.github.andreybgm.artistdiscography.api.response.AlbumSearchResponse;
import io.github.andreybgm.artistdiscography.data.Album;
import io.github.andreybgm.artistdiscography.utils.schedulers.DefaultSchedulerProvider;
import io.reactivex.Observable;
import io.reactivex.Single;

public class Repository implements DataSource {

    private static final String COLLECTION_TYPE_ALBUM = "Album";

    @Override
    public Single<List<Album>> findAlbumsByArtist(String artist) {
        return ApiFactory.getService().albums(artist)
                .map(AlbumSearchResponse::getResults)
                .flatMap(Observable::fromIterable)
                .filter(result -> isAlbum(result) && isResultArtistEqualTo(result, artist))
                .map(this::createAlbumByResult)
                .sorted(this::compareByName)
                .toList()
                .subscribeOn(DefaultSchedulerProvider.getInstance().io());
    }

    private boolean isAlbum(AlbumSearchResponse.Result result) {
        return COLLECTION_TYPE_ALBUM.equalsIgnoreCase(result.getCollectionType());
    }

    private boolean isResultArtistEqualTo(AlbumSearchResponse.Result result, String artist) {
        return artist.equalsIgnoreCase(result.getArtistName());
    }

    private Album createAlbumByResult(AlbumSearchResponse.Result result) {
        return new Album(result.getCollectionName(), result.getArtworkUrl100());
    }

    private int compareByName(Album album1, Album album2) {
        return album1.getTitle().compareTo(album2.getTitle());
    }
}
