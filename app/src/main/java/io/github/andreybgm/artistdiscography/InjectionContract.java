package io.github.andreybgm.artistdiscography;

import android.support.annotation.NonNull;

import io.github.andreybgm.artistdiscography.utils.ImageLoader;
import okhttp3.OkHttpClient;

public interface InjectionContract {
    @NonNull
    OkHttpClient provideOkHttpClient();

    @NonNull
    ImageLoader provideImageLoader();
}
