package io.github.andreybgm.artistdiscography.screen.main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.andreybgm.artistdiscography.R;
import io.github.andreybgm.artistdiscography.repository.RepositoryProvider;
import io.github.andreybgm.artistdiscography.screen.base.RxUtils;
import io.github.andreybgm.artistdiscography.screen.main.uievent.SearchAlbumsEvent;
import io.github.andreybgm.artistdiscography.utils.ScreenUtils;
import io.github.andreybgm.artistdiscography.utils.schedulers.DefaultSchedulerProvider;
import io.reactivex.disposables.CompositeDisposable;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.list_albums)
    RecyclerView albumsView;

    @BindView(R.id.empty_list)
    View emptyListView;

    @BindView(R.id.progress_loading)
    View loadingProgressView;

    @BindView(R.id.error)
    View errorView;

    @BindView(R.id.edit_search)
    EditText searchEditText;

    private MainPresenter presenter;
    private ListAdapter adapter;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private Set<View> allListViews;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        presenter = (MainPresenter) getLastCustomNonConfigurationInstance();

        if (presenter == null) {
            presenter = new MainPresenter.Builder(
                    RepositoryProvider.provideRepository(),
                    DefaultSchedulerProvider.getInstance())
                    .build();
        }

        adapter = new ListAdapter(this, Collections.emptyList());

        albumsView.setHasFixedSize(true);
        albumsView.setAdapter(adapter);
        albumsView.setLayoutManager(new LinearLayoutManager(this));
        albumsView.addItemDecoration(new DividerItemDecoration(this,
                LinearLayoutManager.VERTICAL));

        searchEditText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                performSearch();
                return true;
            }

            return false;
        });

        allListViews = new HashSet<>();
        allListViews.add(albumsView);
        allListViews.add(emptyListView);
        allListViews.add(loadingProgressView);
        allListViews.add(errorView);
    }

    @Override
    public void onStart() {
        super.onStart();

        compositeDisposable.add(
                presenter.getUiModels()
                        .compose(RxUtils.observeOnUiWithDebounce())
                        .subscribe(this::acceptNewUiModel)
        );
    }

    @Override
    public void onStop() {
        super.onStop();

        compositeDisposable.clear();
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return presenter;
    }

    private void acceptNewUiModel(MainUiModel newModel) {
        adapter.changeDataSet(newModel.getAlbums());

        Set<View> visibleViews = new HashSet<>();

        if (newModel.isLoading()) {
            visibleViews.add(loadingProgressView);
        } else if (newModel.isLoadingError()) {
            visibleViews.add(errorView);
        } else if (newModel.getAlbums().isEmpty()) {
            visibleViews.add(emptyListView);
        } else {
            visibleViews.add(albumsView);
        }

        ScreenUtils.remainVisibleViews(allListViews, visibleViews);
    }

    private void performSearch() {
        String artist = searchEditText.getText().toString();
        presenter.sendUiEvent(SearchAlbumsEvent.create(artist));
        ScreenUtils.hideKeyboard(this);
    }
}
