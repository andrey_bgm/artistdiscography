package io.github.andreybgm.artistdiscography.screen.main;

import android.support.annotation.NonNull;

import java.util.Collections;
import java.util.List;

import io.github.andreybgm.artistdiscography.data.Album;
import io.github.andreybgm.artistdiscography.screen.base.UiModel;

public class MainUiModel implements UiModel {

    public static final MainUiModel DEFAULT = MainUiModel.Builder.create().build();

    private final boolean loading;
    private final boolean loadingError;
    @NonNull
    private final List<Album> albums;

    public MainUiModel(Builder builder) {
        this.loading = builder.loading;
        this.loadingError = builder.loadingError;
        this.albums = builder.albums;
    }

    public Builder copy() {
        return new Builder(this);
    }

    public boolean isLoading() {
        return loading;
    }

    public boolean isLoadingError() {
        return loadingError;
    }

    @NonNull
    public List<Album> getAlbums() {
        return albums;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MainUiModel that = (MainUiModel) o;

        if (loading != that.loading) return false;
        if (loadingError != that.loadingError) return false;
        return albums.equals(that.albums);

    }

    @Override
    public int hashCode() {
        int result = (loading ? 1 : 0);
        result = 31 * result + (loadingError ? 1 : 0);
        result = 31 * result + albums.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "MainUiModel{" +
                "loading=" + loading +
                ", loadingError=" + loadingError +
                ", albums=" + albums +
                '}';
    }

    public static class Builder {
        private boolean loading;
        private boolean loadingError;
        @NonNull
        private List<Album> albums;

        public static Builder create() {
            return new Builder();
        }

        public Builder() {
            albums = Collections.emptyList();
        }

        public Builder(MainUiModel model) {
            this.loading = model.loading;
            this.loadingError = model.loadingError;
            this.albums = model.albums;
        }

        public Builder loading(boolean loading) {
            this.loading = loading;
            return this;
        }

        public Builder loadingError(boolean loadingError) {
            this.loadingError = loadingError;
            return this;
        }

        public Builder albums(@NonNull List<Album> albums) {
            this.albums = albums;
            return this;
        }

        public MainUiModel build() {
            return new MainUiModel(this);
        }
    }
}
