package io.github.andreybgm.artistdiscography.screen.main;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import io.github.andreybgm.artistdiscography.R;
import io.github.andreybgm.artistdiscography.data.Album;

public class ListAdapter extends RecyclerView.Adapter<ListViewHolder> {

    private final Context context;
    private List<Album> albums;

    public ListAdapter(Context context, List<Album> albums) {
        this.context = context;
        this.albums = albums;
    }

    @Override
    public ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context)
                .inflate(R.layout.item_main, parent, false);

        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ListViewHolder holder, int position) {
        Album album = albums.get(position);
        holder.bind(album);
    }

    @Override
    public int getItemCount() {
        return albums.size();
    }

    public void changeDataSet(List<Album> albums) {
        this.albums = albums;
        notifyDataSetChanged();
    }
}
