package io.github.andreybgm.artistdiscography.api;

import io.github.andreybgm.artistdiscography.api.response.AlbumSearchResponse;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ITunesService {
    @GET("search?country=ru&media=music&entity=album&attribute=artistTerm&version=2")
    Observable<AlbumSearchResponse> albums(@Query("term") String artist);
}
