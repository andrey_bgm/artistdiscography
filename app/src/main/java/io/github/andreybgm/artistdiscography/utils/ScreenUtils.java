package io.github.andreybgm.artistdiscography.utils;

import android.app.Activity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.util.Set;

import io.reactivex.Observable;

public class ScreenUtils {
    private ScreenUtils() {
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm =
                (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getWindow().getDecorView();
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showView(View view) {
        if (view.getVisibility() != View.VISIBLE) {
            view.setVisibility(View.VISIBLE);
        }
    }

    public static void hideView(View view) {
        if (view.getVisibility() != View.GONE) {
            view.setVisibility(View.GONE);
        }
    }

    public static void remainVisibleViews(Iterable<View> allViews, Set<View> visibleViews) {
        Observable.fromIterable(allViews)
                .filter(view -> !visibleViews.contains(view))
                .blockingForEach(ScreenUtils::hideView);
        Observable.fromIterable(visibleViews)
                .blockingForEach(ScreenUtils::showView);
    }
}
