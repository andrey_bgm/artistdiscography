package io.github.andreybgm.artistdiscography.screen.main;

import android.support.annotation.NonNull;

import java.util.Collections;

import io.github.andreybgm.artistdiscography.repository.DataSource;
import io.github.andreybgm.artistdiscography.screen.base.BasePresenter;
import io.github.andreybgm.artistdiscography.screen.base.BasePresenterBuilder;
import io.github.andreybgm.artistdiscography.screen.base.Result;
import io.github.andreybgm.artistdiscography.screen.base.UiEvent;
import io.github.andreybgm.artistdiscography.screen.main.uievent.SearchAlbumsEvent;
import io.github.andreybgm.artistdiscography.screen.main.uievent.SearchAlbumsResult;
import io.github.andreybgm.artistdiscography.utils.schedulers.SchedulerProvider;
import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;

public class MainPresenter extends BasePresenter<MainUiModel> {
    public MainPresenter(@NonNull BasePresenterBuilder builder) {
        super(builder);

        connect();
    }

    @Override
    protected MainUiModel getInitialUiModel() {
        return MainUiModel.DEFAULT;
    }

    @Override
    protected Observable<Result> handleEvents(Observable<UiEvent> events) {
        ObservableTransformer<SearchAlbumsEvent, Result> searchAlbums = searchEvents ->
                searchEvents.flatMap(event ->
                        getRepository().findAlbumsByArtist(event.getArtist())
                                .map(SearchAlbumsResult::success)
                                .onErrorReturn(SearchAlbumsResult::error)
                                .toObservable()
                                .startWith(SearchAlbumsResult.IN_PROGRESS)
                );

        return events.publish(sharedEvents -> Observable.merge(
                handleEventsOfClass(sharedEvents, SearchAlbumsEvent.class, searchAlbums),
                sharedEvents.ofType(Result.class))
        );
    }

    @Override
    protected MainUiModel reduceModel(MainUiModel model, Result result) {
        if (result instanceof SearchAlbumsResult) {
            return reduceModel(model, (SearchAlbumsResult) result);
        }

        throw makeUnknownResultException(result);
    }

    private MainUiModel reduceModel(MainUiModel model, SearchAlbumsResult result) {
        if (result.isInProgress()) {
            return model.copy()
                    .loading(true)
                    .loadingError(false)
                    .build();
        } else if (result.isError()) {
            return model.copy()
                    .loading(false)
                    .loadingError(true)
                    .albums(Collections.emptyList())
                    .build();
        } else {
            return model.copy()
                    .loading(false)
                    .loadingError(false)
                    .albums(result.getAlbums())
                    .build();
        }
    }

    public static class Builder extends BasePresenterBuilder<Builder, MainPresenter, MainUiModel> {
        public Builder(DataSource repository, SchedulerProvider schedulerProvider) {
            super(repository, schedulerProvider);
        }

        @Override
        @NonNull
        public MainPresenter build() {
            return new MainPresenter(this);
        }

        @NonNull
        @Override
        public Builder getThis() {
            return this;
        }
    }
}
