package io.github.andreybgm.artistdiscography.screen.main;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.andreybgm.artistdiscography.Injection;
import io.github.andreybgm.artistdiscography.R;
import io.github.andreybgm.artistdiscography.data.Album;
import io.github.andreybgm.artistdiscography.utils.ImageLoader;

public class ListViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.text_title)
    TextView titleTextView;

    @BindView(R.id.image_cover)
    ImageView coverImageView;

    private final ImageLoader imageLoader;

    public ListViewHolder(View itemView) {
        super(itemView);

        ButterKnife.bind(this, itemView);
        imageLoader = Injection.provideInjection().provideImageLoader();
    }

    public void bind(Album album) {
        titleTextView.setText(album.getTitle());
        imageLoader.loadAlbumCover(itemView.getContext(), album, coverImageView);
    }
}
