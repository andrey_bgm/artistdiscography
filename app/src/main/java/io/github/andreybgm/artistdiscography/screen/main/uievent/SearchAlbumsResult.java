package io.github.andreybgm.artistdiscography.screen.main.uievent;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Collections;
import java.util.List;

import io.github.andreybgm.artistdiscography.data.Album;
import io.github.andreybgm.artistdiscography.screen.base.event.BooleanResult;

public class SearchAlbumsResult extends BooleanResult {
    public static final SearchAlbumsResult IN_PROGRESS =
            new SearchAlbumsResult(State.IN_PROGRESS, null, Collections.emptyList());

    public static SearchAlbumsResult error(@NonNull Throwable error) {
        return new SearchAlbumsResult(State.ERROR, error, Collections.emptyList());
    }

    public static SearchAlbumsResult success(@NonNull List<Album> artists) {
        return new SearchAlbumsResult(State.SUCCESS, null, artists);
    }

    @NonNull
    private final List<Album> albums;

    private SearchAlbumsResult(@NonNull State state, @Nullable Throwable error,
                               @NonNull List<Album> albums) {
        super(state, error);
        this.albums = albums;
    }

    @NonNull
    public List<Album> getAlbums() {
        return albums;
    }
}
