package io.github.andreybgm.artistdiscography.utils;

import android.content.Context;
import android.widget.ImageView;

import io.github.andreybgm.artistdiscography.data.Album;

public interface ImageLoader {
    void loadAlbumCover(Context context, Album album, ImageView imageView);
}
