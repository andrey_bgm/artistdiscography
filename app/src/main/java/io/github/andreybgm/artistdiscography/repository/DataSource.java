package io.github.andreybgm.artistdiscography.repository;

import java.util.List;

import io.github.andreybgm.artistdiscography.data.Album;
import io.reactivex.Single;

public interface DataSource {
    Single<List<Album>> findAlbumsByArtist(String artist);
}
