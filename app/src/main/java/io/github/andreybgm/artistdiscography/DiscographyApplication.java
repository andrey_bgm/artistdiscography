package io.github.andreybgm.artistdiscography;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;

public class DiscographyApplication extends Application {
    @SuppressLint("StaticFieldLeak")
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();

        context = this;
    }

    public static Context getContext() {
        return context;
    }
}
