package io.github.andreybgm.artistdiscography.api.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AlbumSearchResponse {
    @SerializedName("results")
    private List<Result> results;

    public List<Result> getResults() {
        return results;
    }

    public static class Result {
        @SerializedName("collectionType")
        private String collectionType;

        @SerializedName("artistName")
        private String artistName;

        @SerializedName("collectionName")
        private String collectionName;

        @SerializedName("artworkUrl100")
        private String artworkUrl100;

        public String getCollectionType() {
            return collectionType;
        }

        public String getArtistName() {
            return artistName;
        }

        public String getCollectionName() {
            return collectionName;
        }

        public String getArtworkUrl100() {
            return artworkUrl100;
        }
    }
}
