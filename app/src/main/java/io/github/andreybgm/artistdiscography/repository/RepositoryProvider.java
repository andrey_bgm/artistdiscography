package io.github.andreybgm.artistdiscography.repository;

public class RepositoryProvider {
    private static DataSource repository;

    public static synchronized DataSource provideRepository() {
        if (repository == null) {
            repository = new Repository();
        }

        return repository;
    }
}
