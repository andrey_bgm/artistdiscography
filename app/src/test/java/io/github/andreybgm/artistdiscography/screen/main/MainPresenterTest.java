package io.github.andreybgm.artistdiscography.screen.main;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;

import io.github.andreybgm.artistdiscography.data.Album;
import io.github.andreybgm.artistdiscography.repository.DataSource;
import io.github.andreybgm.artistdiscography.screen.main.uievent.SearchAlbumsEvent;
import io.github.andreybgm.artistdiscography.utils.schedulers.ImmediateSchedulerProvider;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

public class MainPresenterTest {
    private static final String ARTIST_NAME = "Artist1";

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private DataSource repository;

    private MainPresenter presenter;
    private Album album;

    @Before
    public void setUp() throws Exception {
        presenter = new MainPresenter.Builder(repository, new ImmediateSchedulerProvider())
                .build();
        album = new Album("Album1", "http://example.com/img1.jpg");
    }

    @Test
    public void getDefaultUiModel() throws Exception {
        presenter.getUiModels()
                .test()
                .assertNoErrors()
                .assertNotComplete()
                .assertValue(MainUiModel.DEFAULT);
    }

    @Test
    public void searchAlbums() throws Exception {
        when(repository.findAlbumsByArtist(ARTIST_NAME)).thenReturn(Single.just(
                Collections.singletonList(album)));
        TestObserver<MainUiModel> testObserver = presenter.getUiModels()
                .skip(1)
                .test();

        presenter.sendUiEvent(SearchAlbumsEvent.create(ARTIST_NAME));

        testObserver
                .awaitCount(2)
                .assertNoErrors()
                .assertValueAt(0, model -> {
                    assertThat(model.isLoading()).isTrue();
                    assertThat(model.isLoadingError()).isFalse();
                    assertThat(model.getAlbums()).isEmpty();

                    return true;
                })
                .assertValueAt(1, model -> {
                    assertThat(model.isLoading()).isFalse();
                    assertThat(model.isLoadingError()).isFalse();
                    assertThat(model.getAlbums())
                            .hasSize(1)
                            .contains(album);

                    return true;
                });
    }

    @Test
    public void searchAlbumsWhenError() throws Exception {
        when(repository.findAlbumsByArtist(ARTIST_NAME)).thenReturn(Single.error(
                new RuntimeException()));

        presenter.sendUiEvent(SearchAlbumsEvent.create(ARTIST_NAME));

        presenter.getUiModels()
                .test()
                .assertNoErrors()
                .assertValue(model -> {
                    assertThat(model.isLoading()).isFalse();
                    assertThat(model.isLoadingError()).isTrue();
                    assertThat(model.getAlbums()).isEmpty();

                    return true;
                });
    }
}